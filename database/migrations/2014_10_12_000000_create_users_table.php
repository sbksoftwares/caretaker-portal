<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('bussiness_domain')->index()->nullable();
            $table->string('bussiness_subdomain')->index()->nullable();
            $table->string('bussiness_hash')->index()->nullable();
            $table->string('bussiness_address');
            $table->string('bussiness_contact');
            $table->string('bussiness_name');
            $table->string('status')->default('inactive'); // active | inactive
            $table->string('owner_name');
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
