@extends('layouts.app')

@section('content')

@include('dashboard.subscriptions')
@include('dashboard.subscription')
@include('dashboard.account')
@include('dashboard.domain')
@include('dashboard.branch')
@include('dashboard.modal')
@include('dashboard.download')

@endsection
