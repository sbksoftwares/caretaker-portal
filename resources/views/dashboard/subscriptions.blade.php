<div class="container" id="subscriptions">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3><small>Bussiness Branch Subscriptions</small></h3>
                </div>

                <div class="card-body">
                    <div class="">
                        @if ($subdomain)
                            <table class="table">
                              <thead class="thead-light">
                                <tr>
                                  <th scope="col">Branch</th>
                                  <th scope="col">Renewed On</th>
                                  <th scope="col">Expires On</th>
                                  <th scope="col">Status</th>
                                  <th scope="col" colspan="3" style="text-align:center">Action</th>
                                </tr>
                              </thead>
                              <tbody>
                                  @forelse ($subscriptions as $key => $value)
                                      <tr>
                                        <th scope="row">Accra</th>
                                        <td>20th March</td>
                                        <td>20th May</td>
                                        <td><span class="badge badge-success">Active</span></td>
                                        <td><a class="bait" href="#" data-action="alter-branch-status" data-argv="branchID">{{ '$branch->status' == 'inactive' ? 'Activate' : 'Deactivate' }}</a></td>
                                        <td><a class="bait" href="#" data-action="goto-branch" data-argv="branchID">Open</a></td>
                                        <td><a class="bait" href="#" data-action="show-branch-detail" data-argv="branchID">View More ...</a></td>
                                      </tr>
                                  @empty
                                      <tr>
                                          <td colspan="7">
                                              You haven't created any subscriptions yet, you need to create at least one subscription
                                          </td>
                                          </tr>
                                  @endforelse

                              </tbody>
                            </table>
                            <button type="button" class="bait btn btn-primary float-right" data-argv="branch" data-action="show-create-branch">Add Branch</button>
                        @else
                            <p>
                                First things first. First we need to set you up with a subdomain for your bussiness.
                                <button type="button" class="bait btn btn-warning float-right" data-argv="domain" data-action="show-sub">Create my subdomain</button>
                            </p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
