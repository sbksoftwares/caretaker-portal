<div class="container lost" id="account">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3><small>Account Settings</small></h3>
                </div>

                <div class="card-body">
                    <form method="POST" action="#" id="user-form">
                        @csrf

                        <div class="form-group row">
                            <label for="bussiness_name" class="col-md-4 col-form-label text-md-right">{{ __('Name of Bussiness') }}</label>

                            <div class="col-md-6">
                                <input id="bussiness_name" type="text" class="form-control{{ $errors->has('bussiness_name') ? ' is-invalid' : '' }}" name="bussiness_name" value="{{ old('bussiness_name') }}" required>

                                @if ($errors->has('bussiness_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('bussiness_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="owner_name" class="col-md-4 col-form-label text-md-right">{{ __('Name of Owner') }}</label>

                            <div class="col-md-6">
                                <input id="owner_name" type="text" class="form-control{{ $errors->has('owner_name') ? ' is-invalid' : '' }}" name="owner_name" value="{{ old('owner_name') }}" required>

                                @if ($errors->has('owner_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('owner_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="bussiness_address" class="col-md-4 col-form-label text-md-right">{{ __('Bussiness Address') }}</label>

                            <div class="col-md-6">
                                <input id="bussiness_address" type="text" class="form-control{{ $errors->has('bussiness_address') ? ' is-invalid' : '' }}" name="bussiness_address" value="{{ old('bussiness_address') }}" required>

                                @if ($errors->has('bussiness_address'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('bussiness_address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="bussiness_contact" class="col-md-4 col-form-label text-md-right">{{ __('Bussiness Contact Number') }}</label>

                            <div class="col-md-6">
                                <input id="bussiness_contact" type="text" class="form-control{{ $errors->has('bussiness_contact') ? ' is-invalid' : '' }}" name="bussiness_contact" value="{{ old('bussiness_contact') }}" required>

                                @if ($errors->has('bussiness_contact'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('bussiness_contact') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" readonly>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Admin Username') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" readonly>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary bait" data-argv="user-form" data-action="update-account">
                                    {{ __('Update') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
