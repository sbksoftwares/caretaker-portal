<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalTitle">Bussiness Branch Subscription Details <small>( Branch Name )</small> </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Aliquip amet laboris dolor proident duis in mollit veniam excepteur ex culpa velit. Sint nisi officia in velit ipsum id exercitation elit proident commodo officia commodo nisi. Ut quis eu enim incididunt irure ad excepteur anim eu culpa. Aliquip dolore velit culpa tempor fugiat adipisicing dolore ea adipisicing enim ipsum nulla mollit laborum. Aliqua nisi aliqua ad aliqua culpa cillum amet aliquip qui non sunt sunt commodo. Ex in aute enim aliqua veniam quis irure dolore velit eu ex eu id laboris esse aliquip. Pariatur dolor velit deserunt qui Lorem ex magna nulla veniam officia in ut. Cupidatat id pariatur ad qui in nisi ad esse adipisicing ea consectetur pariatur aliquip adipisicing.</p>
        <p>In amet pariatur anim laboris non nulla aliqua cillum enim veniam. Exercitation duis aliquip sint in aute culpa commodo elit sit. Do nostrud quis laborum incididunt consequat aute ea laborum eu eiusmod ut in. Quis id enim amet deserunt laborum ex ipsum irure non tempor esse ex nostrud non.</p>
        <p>Esse eu sint dolore proident occaecat ad officia nisi nostrud ipsum. Aliquip eu ullamco commodo ea amet qui cillum fugiat mollit aute nisi. Est adipisicing duis nulla consectetur sit labore id pariatur sit laboris. Eiusmod mollit Lorem elit officia id nulla et cillum in culpa in laborum ut. Consequat esse laboris dolor commodo aliqua dolor nulla.</p>
        <p>Velit dolor consequat ex sit qui exercitation cupidatat et. Culpa fugiat occaecat dolor nulla mollit ut quis exercitation pariatur cillum eiusmod irure. Quis consectetur elit qui velit laboris consectetur deserunt id ullamco voluptate proident ullamco anim in.</p>
      </div>
      <div class="modal-footer">
        <button type="button" data-action="show-update-branch" data-argv="branch-ID" class="btn btn-primary bait">Edit Branch</button>
        <button type="button" data-action="delete-branch" data-argv="branchID" class="btn btn-danger">Delete Branch</button>
        <button type="button" data-action="show-sub" data-argv="subscription-branch" class="btn btn-success bait">Extend Subscription</button>
        <button type="button" data-action="show-download" data-argv="download-branch" class="btn btn-info bait">Download Offline Version for Branch</button>
      </div>
    </div>
  </div>
</div>
