<div class="container lost" id="subscription">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3><small>Subscription Form</small></h3>
                </div>

                <div class="card-body">
                    <form method="POST" action="#" id="sub-form">
                        @csrf
                        <input type="hidden" id="branch_id" name="branch_id" readonly>
                        <div class="form-group row">
                            <label for="end_date" class="col-md-4 col-form-label text-md-right">{{ __('Duration') }}</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <select class="custom-select">
                                      <option value="1">1</option>
                                      <option value="2">2</option>
                                      <option value="3">3</option>
                                      <option value="1">4</option>
                                      <option value="2">5</option>
                                      <option value="3">6</option>
                                      <option value="1">7</option>
                                      <option value="2">8</option>
                                      <option value="3">9</option>
                                      <option value="1">10</option>
                                      <option value="2">11</option>
                                    </select>
                                    <select class="custom-select">
                                      <option value="month">Month(s)</option>
                                      <option value="year">Year(s)</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary bait" data-argv="sub-form" data-action="create-sub">
                                    {{ __('Subscribe') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
