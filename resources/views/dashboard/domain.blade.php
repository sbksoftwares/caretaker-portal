<div class="container lost" id="domain">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3><small>Domain Settings</small> </h3>
                </div>

                <div class="card-body">
                    <form method="POST" action="#" id="subdomain-form">
                        @csrf

                        <div class="form-group row">

                            <div class="col-md-6 offset-md-4">
                                <p>Set a subdomain for your bussiness ...</p>
                                <div class="input-group mb-3">
                                  <input  id="bussiness_subdomain" type="text" class="form-control" placeholder="subdomain" name="bussiness_subdomain" required>
                                  <div class="input-group-append">
                                    <span class="input-group-text">.caretakerpos.com</span>
                                  </div>
                                </div>
                                <span class="invalid-feedback" role="alert">
                                    <strong></strong>
                                </span>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary bait" data-argv="subdomain-form" data-action="set-subdomain">
                                    {{ __('Set Subdomain') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    {{-- <br>
                    <br>
                    <br>
                    <form method="POST" action="#" id="domain-form">
                        @csrf
                        <div class="form-group row">

                            <div class="col-md-6 offset-md-4">
                                <p>You can also use your own domain name ...</p>
                                <input id="bussiness_domain" type="text" class="form-control" name="bussiness_domain" placeholder="Bussiness Domain e.g. (wwww.mydomain.com)" required>

                                <span class="invalid-feedback" role="alert">
                                    <strong></strong>
                                </span>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary bait" data-argv="domain-form" data-action="set-domain">
                                    {{ __('Add Domain') }}
                                </button>
                            </div>
                        </div>
                    </form> --}}
                </div>
            </div>
        </div>
    </div>
</div>
