<div class="container lost" id="branch">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3><small>Bussiness Branch Form</small></h3>
                </div>

                <div class="card-body">
                    <form method="POST" action="#" id="branch-form">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Branch Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name">

                                <span class="invalid-feedback" role="alert">
                                    <strong></strong>
                                </span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="location" class="col-md-4 col-form-label text-md-right">{{ __('Branch Location') }}</label>

                            <div class="col-md-6">
                                <input id="location" type="text" class="form-control" name="location">

                                <span class="invalid-feedback" role="alert">
                                    <strong></strong>
                                </span>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary lost bait" data-argv="branch-form" data-action="create-branch" id="create-branch-btn">
                                    {{ __('Create') }}
                                </button>
                                <button type="submit" class="btn btn-primary lost bait" data-argv="branch-form" data-action="update-branch" id="update-branch-btn">
                                    {{ __('Update') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
