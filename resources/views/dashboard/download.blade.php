<div class="container lost" id="download">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3><small>Application Excecutable Download</small></h3>
                </div>

                <div class="card-body">
                    <ol>
                        <li>Main Application Excecutable</li>
                        <li>Activation File (Contains Encrypted User Information Needed to Set Up Initial Database and Link to Online Portal)</li>
                        <li>Activation Key (Basically the Decryption Key for the Above File.)</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
