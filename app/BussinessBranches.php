<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Downloads;
use App\Subscriptions;

class BussinessBranches extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'location', 'status', 'branch_hash', 'expires_on', 'renewed_on'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function downloads(){
        return $this->hasMany(Downloads::class, 'branch_id');
    }

    public function subscriptions(){
        return $this->hasMany(Subscriptions::class, 'branch_id');
    }
}
