<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\BussinessBranches;
use App\Custom\JsonRequest;
use App\Custom\Helpers;

class UserController extends Controller
{
    //

    public function setSubdomain(Request $request)
    {
        $user = $request->user();
        $subdomain = $request->bussiness_subdomain;
        $subdomain_users = User::where('bussiness_subdomain', $subdomain)->get();
        if ($subdomain_users->isNotEmpty()) {
            $res = [
                    'status' => false,
                    'msg' => 'subdomain already exists',
            ];
        } else {
            if ($user->bussiness_subdomain) {
                $res = [
                        'status' => false,
                        'msg' => 'this bussinesss already has a subdomain',
                ];
            } else {

                $subdomain_hash = base64_encode(md5($subdomain));

                $params = [
                    'KEY' => env('ESTABLISH_BUSSINESS_KEY'),
                    'name' => $user->bussiness_name,
                    'domain' => $user->domain,
                    'subdomain' => $subdomain,
                    'subdomain_hash' => $subdomain_hash,
                    'email' => $user->email,
                    'api' => true,
                ];

                $path = 'establish/bussiness';
                $req = new JsonRequest($path, $params);
                $res = $req->post();

                if ($res['status']) {
                    $user->bussiness_subdomain = $subdomain;
                    $user->bussiness_hash = $hash;
                    $user->save();
                }

            }
        }

        return response()->json($res);
    }

    public function addBranch(Request $request)
    {
        $user = $request->user();
        $branch_hash = base64_encode(md5($request->name+$user->id));
        $branches = BussinessBranches::where('branch_hash', $branch_hash)->get();
        if ($branches->isNotEmpty()) {
            $res = [
                    'status' => false,
                    'msg' => 'branch already exists',
            ];
        } else {

            $renewed_on = new DateTime();
            $expires_on = Helpers::DateDiff($renewed_on, '1 month');

            $params = [
                'KEY' => env('ESTABLISH_BRANCH_KEY'),
                'subdomain_hash' => $user->subdomain_hash,
                'name' => $request->name,
                'location' => $request->location,
                'status' => 'inactive',
                'branch_hash' => $branch_hash,
                'renewed_on' => $renewed_on->format('Y-m-d'),
                'expires_on' => $expires_on->format('Y-m-d'),
                'api' => true,
            ];

            $path = 'establish/branch';
            $req = new JsonRequest($path, $params);
            $res = $req->post();

            if ($res['status']) {
                $user->bussiness_branches()->create([
                    'name' => $request->name,
                    'location' => $request->location,
                    'status' => 'inactive',
                    'branch_hash' => $branch_hash,
                    'renewed_on' => $renewed_on->format('Y-m-d'),
                    'expires_on' => $expires_on->format('Y-m-d'),
                ]);
            }

        }

        return response()->json($res);
    }

    public function extendSubscription(Request $request)
    {
        $user = $request->user();
        $branch = BussinessBranches::where('id', $request->branch_id)->get();
        if ($branch->isEmpty()) {
            $res = [
                    'status' => false,
                    'msg' => 'branch does not exist',
            ];
        } else {

            $renewed_on = new DateTime();

            $start_date = $branch->expires_on ? new DateTime($branch->expires_on) : $renewed_on;

            $expires_on = Helpers::DateDiff($start_date, $request->expires_on);

            $params = [
                'KEY' => env('EXTEND_SUBSCRIPTION'),
                'subdomain_hash' => $user->subdomain_hash,
                'branch_hash' => $branch->branch_hash,
                'renewed_on' => $renewed_on->format('Y-m-d'),
                'expires_on' => $expires_on->format('Y-m-d'),
                'api' => true,
            ];

            $path = 'extend/subscription';
            $req = new JsonRequest($path, $params);
            $res = $req->post();

            if ($res['status']) {
                $branch->expires_on = $expires_on->format('Y-m-d');
                $branch->renewed_on = $renewed_on->format('Y-m-d');
                $branch->save();
                $branch->subscriptions()->create([
                    'start_date' => $renewed_on->format('Y-m-d'),
                    'end_date' => $expires_on->format('Y-m-d'),
                    'amount' => Helpers::AmountPeriod($request->expires_on),
                ]);
            }

        }

        return response()->json($res);
    }

    public function updateBussiness(Request $request)
    {
        $user = $request->user();

        $params = $request->all();
        $params['KEY'] = env('UPDATE_BUSSINESS');
        $params['api'] = true;
        $params['subdomain_hash'] = $user->subdomain_hash;

        $path = 'update/bussiness';
        $req = new JsonRequest($path, $params);
        $res = $req->post();

        if ($res['status']) {
            $user->update($request->all());
        }

        return response()->json($res);
    }

    public function updateBranch(Request $request)
    {

        $branch = BussinessBranches::where('id', $request->branch_id)->get();
        $params = $request->all();
        $params['KEY'] = env('UPDATE_BRANCH');
        $params['api'] = true;
        $params['branch_hash'] = $branch->branch_hash;

        $path = 'update/branch';
        $req = new JsonRequest($path, $params);
        $res = $req->post();

        if ($res['status']) {
            $branch->update($request->all());
        }

        return response()->json($res);
    }
}
