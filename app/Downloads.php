<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\BussinessBranches;

class Downloads extends Model
{

    public function bussiness_branch(){
        return $this->belongsTo(BussinessBranches::class, 'branch_id');
    }
}
