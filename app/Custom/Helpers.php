<?php

namespace App\Custom\Helpers;


/**
 *
 */

class Helpers
{

    const PAYMENT_RATE = 50;

    public static function DateDiff($start, $diff)
    {
        $interval = DateInterval::createFromDateString($diff);
        $end = $start->add($interval);
        return $end;
    }

    public static function AmountPeriod($period)
    {
        $period = explode($period, ' ');
        if ($period[1] == 'year') {
            $months = 12 * (int)$period[0];
        }else{
            $months = (int)$period[0];
        }

        $amount = $months * PAYMENT_RATE;

        return $amount;
    }

}
