<?php

namespace App\Custom;
/**
 *
 */
use GuzzleHttp\Client;

class JsonRequest
{

    function __construct($path, $params)
    {
        $this->path = $path;
        $this->params = $params;
        $base_uri = env('CARETAKER_BASE_URI');
        $this->client = new Client(['base_uri' => $base_uri]);
    }

    private function request($verb, $data)
    {
        $response = $this->client->request($verb, $this->path, $data);

        if ($response->getStatusCode() != 200) {
            $response = [
                'status' => false,
                'code' => $response->getStatusCode(),
                'msg' => 'External Request Not Ok',
            ];
        } else {
            $response = json_decode($response->getBody());
            $response = [
                    'status' => $response->status,
                    'msg' => $response->msg
            ];
        }


        return $response;
    }

    public function get()
    {
        $verb = 'GET';
        $data = $this->params;
        return $this->request($verb, $data);
    }

    public function post()
    {
        $verb = 'POST';
        $data = ['form_params'=>$this->params];
        return $this->request($verb, $data);
    }

}
