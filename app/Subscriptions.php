<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\BussinessBranches;

class Subscriptions extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'start_date', 'end_date', 'amount'
    ];

    public function bussiness_branch(){
        return $this->belongsTo(BussinessBranches::class, 'branch_id');
    }
}
