var current_view = 'subscriptions';

var actions = {
    'show-sub':function (view) {
        show(view);
    },
    'show-create-branch':function (view) {
        show(view);
        $('#branch-form input').val("");
        $('#create-branch-btn').show();
        $('#update-branch-btn').hide();
        return true;
    },
    'show-update-branch':function (branch) {
        var view = branch.split('-')[0];
        var id = branch.split('-')[1];
        show(view);
        $('#branch-form #name').val("Name");
        $('#branch-form #location').val("Location");
        $('#create-branch-btn').hide();
        $('#update-branch-btn').show();
        return true;
    },
    'show-branch-detail':function (branch) {
        $('#modal').modal();
    },
    'show-download':function (branch) {
        var view = branch.split('-')[0];
        var id = branch.split('-')[1];
        show(view);
    },
    'alter-branch-status':function (branch) {

    },
    'goto-branch':function (branch) {
        window.open('http://care-taker.herokuapp.com');
    },
    'show-nav':function (view) {
        show(view);
        return true;
    },
    'update-account':function (form) {
        url = '';
        submit(form, url);
        return true;
    },
    'set-subdomain':function (form) {
        url = '/subdomain/set';
        submit(url, form, function (data) {
            console.log(data);
        });
        return true;
    },
    'set-domain':function (form) {
        url = '';
        submit(form, url);
        return true;
    },
    'create-branch':function (form) {
        url = '';
        submit(form, url);
        return true;
    },
    'update-branch':function (form) {
        url = '';
        submit(form, url);
        return true;
    },
    'create-sub':function (form) {
        url = '';
        submit(form, url);
        return true;
    }
}

function executeCallBack(callback, argv) {
    if (typeof callback === "function") {
        // Call it, since we have confirmed it is callable​
        return callback(argv);
    } else {
        console.log("not callback");
        return false;
    }
}

function show(view){
    $('#'+current_view).fadeOut(function () {
        $('#'+view).fadeIn();
        current_view = view;
    });
}

function submit(url, form, callback){
    data = $('#'+form).serialize();
    $.post(url, data, callback);
}

$(document).ready(function () {
    // $('#modal').modal();
    $(document).on('click', '.bait', function (event) {
        event.preventDefault();
        $('#modal').modal('hide');
        var action = $(this).data('action');
        var argv = $(this).data('argv')

        callback = actions[action];
        status = executeCallBack(callback, argv);
        if (status) {
            console.info('callback excecution successfull ...');
        } else {
            console.info('callback excecution failed ...')
        }
    });
});
